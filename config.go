package main

import (
	"log"
	"code.google.com/p/gcfg"
)

type Config struct {
    Server struct { 
        Listen string
        Log string
        Secret string
        CacheDir string
    }
    Filter struct {
        Base, Regex string
    }
    Project map[string]*struct { 
    	Cache string
    }
}

func LoadConfig(filename string) *Config {
    conf := new(Config)

    err := gcfg.ReadFileInto(conf, filename)
    if err != nil {
       log.Fatalf("Failed to parse config file: %s", err)
    }
    
    return conf
}