# README #

Picky - fast and modern image resizer written in GO lang.

Back compatible with [nginx mod image filter](http://nginx.org/ru/docs/http/ngx_http_image_filter_module.html)

## Why picky? ##

Picky uses imagemagick, so it creates better quality thumbnails, it is fast and easy to use \o

### How do I get set up? ###

* build code, move binary somewhere, then run 

There are some steps below to build **picky**

First you need to set up golang and configure enviroment, see [go getting started page](http://golang.org/doc/install#install)

```
#!bash

$ apt-get install libmagickwand-dev

$ mkdir -p $GOPATH/src/bitbucket.org/zaytsew_ilya/picky

$ git clone https://bitbucket.org/zaytsew_ilya/picky.git $GOPATH/src/bitbucket.org/zaytsew_ilya/picky

$ cd $GOPATH/src/bitbucket.org/zaytsew_ilya/picky

$ go get code.google.com/p/gcfg

$ go get github.com/gographics/imagick/imagick

$ go install

```

ready to deploy!

### config example ###


```
#!

[server]
listen = 127.0.0.1:8085
log = /var/log/picky.log
secret = secret
cacheDir = /var/cache/picky

[filter]
base = /home/sandbox/stc1/
regex = ^([cri])/([^/]+)/(?:([0-9]+|-)x([0-9]+|-)/)?([^/]+)/(.+)
```


## config desciption ##
may be you can't turn off logging

set **secret** phrase, it used to prevent DOS

**cacheDir** - where to store resized images



**base** - root direcroty for original images

**regex** - how to parse request url

$1 - method used c - crop, r - resize, i - original image

$2 - secure hash

$3 - width

$4 - height

$5 - project name

$6 - path to file from base

## generatin url ##


```
#!php

public function getUrl($file_path, $mode = 'i', $width = '-', $height = '-')
{
    $secret = 'secret';
    $baseUrl = 'http://picky.example.com/';

    if($mode == 'i')
    {
        $url_path = $file_path;
    }
    else
        $url_path = $width.'x'.$height.'/'.$file_path;

    $secure_hash = md5($url_path.$secret);

    return $baseUrl.$mode.'/'.$secure_hash.'/'.$url_path;
}
```



### usage ###
run:
```
#!bash

$ picky -conf=/path/to/picky.conf

```

##P.S.##
Memory is leaking ¯\\_(ツ)_/¯
