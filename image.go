package main

import (
	"github.com/gographics/imagick/imagick"
)

type Image struct {
	mw *imagick.MagickWand
}

func (i *Image) Resize(rWidth, rHeight uint) error {
	w0 := i.mw.GetImageWidth()
	h0 := i.mw.GetImageHeight()

	if rWidth == 0 {
		rWidth = uint(w0 * rHeight / h0)
	}

	if rHeight == 0 {
		rHeight = uint(h0 * rWidth / w0)
	}

	// Resize the image using the Lanczos filter
	// The blur factor is a float, where > 1 is blurry, < 1 is sharp
	err := i.mw.ResizeImage(rWidth, rHeight, imagick.FILTER_LANCZOS, 1)
	if err != nil {
		return err
	}

	return nil
}

func (i *Image) Crop(rWidth, rHeight uint) error {
	w0 := i.mw.GetImageWidth()
	h0 := i.mw.GetImageHeight()

	var (
		newWidth  uint
		newHeight uint
	)

	aspect_w := float64(rWidth) / float64(w0)
	aspect_h := float64(rHeight) / float64(h0)

	if aspect_w > aspect_h {
		newWidth = w0
		newHeight = uint(w0 * rHeight / rWidth)
	} else {
		newWidth = uint(h0 * rWidth / rHeight)
		newHeight = h0
	}

	var err error

	// Crop image
	err = i.mw.CropImage(newWidth, newHeight, int((w0-newWidth)/2), int((h0-newHeight)/2))
	if err != nil {
		return err
	}

	// Resize image
	err = i.mw.ResizeImage(rWidth, rHeight, imagick.FILTER_LANCZOS, 1)
	if err != nil {
		return err
	}

	return nil
}

func (i *Image) Compress(quality uint) error {
	err := i.mw.SetImageCompressionQuality(quality)
	if err != nil {
		return err
	}
	return nil
}

func (i *Image) GetBlob() []byte {
	return i.mw.GetImageBlob()
}
