package main

import (
	"crypto/md5"
	"fmt"
	"github.com/gographics/imagick/imagick"
	"io/ioutil"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

const (
	M_RESIZE  string = "r"
	M_CROP    string = "c"
	M_INITIAL string = "i"
)

type Request struct {
	Method  string
	Code    string
	Width   string
	Height  string
	Project string
	Path    string
}

func (r *Request) Key() string {
	return strings.Join([]string{r.Method, r.Width + "x" + r.Height, r.Path}, "/")
}

func (r *Request) strtoint(str string) uint {
	if str == "-" {
		return 0
	} else {
		res, _ := strconv.ParseInt(str, 10, 32)
		return uint(res)
	}
}

func (r *Request) WidthInt() uint  { return r.strtoint(r.Width) }
func (r *Request) HeightInt() uint { return r.strtoint(r.Height) }
func (r *Request) Mime() string {
	return mime.TypeByExtension(filepath.Ext(r.Path))
}

func NewRequest(params []string) (*Request, *FilterError) {
	request := new(Request)
	request.Method = params[1]
	request.Code = params[2]

	if request.Method == "i" {
		request.Project = params[5]
		request.Path = params[6]
	} else {
		request.Width = params[3]
		request.Height = params[4]
		request.Project = params[5]
		request.Path = params[6]

		//validate sizes
		if request.Width == "-" && request.Height == "-" {
			return nil, &FilterError{"Sizes not provided", 400}
		}
	}

	if request.Valid() == false {
		return nil, &FilterError{"Code not valid", 400}
	}

	//validate image file exists
	if _, err := os.Stat(config.Filter.Base + request.Project + "/" + request.Path); os.IsNotExist(err) {
		return nil, &FilterError{"File not found", 404}
	}

	return request, nil
}

func (r *Request) Valid() bool {
	var path string
	h := md5.New()

	if r.Method == "i" {
		path = r.Project + "/" + r.Path
	} else {
		path = r.Width + "x" + r.Height + "/" + r.Project + "/" + r.Path
	}

	h.Write([]byte(path + config.Server.Secret))
	return r.Code == fmt.Sprintf("%x", h.Sum(nil))
}

func (r *Request) Name() string {
	return config.Filter.Base + r.Project + "/" + r.Path
}

func (r *Request) CacheName() string {
	cache_path := config.Server.CacheDir + "/" + r.Project + "/" + r.Method + r.Width + "x" + r.Height
	return cache_path + "/" + r.Path
}

func (r *Request) Execute(w http.ResponseWriter, req *http.Request) {
	var name string

	if r.Method == M_INITIAL {
		name = r.Name()
	} else {
		if has := cache.Has(r.CacheName()); has == false {

			file, err := ioutil.ReadFile(r.Name())

			imagick.Initialize()

			image := new(Image)
			image.mw = imagick.NewMagickWand()

			err = image.mw.ReadImageBlob(file)
			if err != nil {
				http.Error(w, "Error occurred", http.StatusInternalServerError)
			}

			switch r.Method {
			case "r":
				err = image.Resize(r.WidthInt(), r.HeightInt())
			case "c":
				err = image.Crop(r.WidthInt(), r.HeightInt())
			}

			if err != nil {
				http.Error(w, "Error occurred", http.StatusInternalServerError)
			}

			cache.Add(r.CacheName(), image.GetBlob())
		}

		name = r.CacheName()
	}

	http.ServeFile(w, req, name)
}
