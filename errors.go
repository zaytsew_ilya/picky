package main

type FilterError struct {
    Message string
    Code    int
}

func (e *FilterError) Error() string { 
    return e.Message 
}