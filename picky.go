package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"path"
	"regexp"
)

var (
	configFile string
	config     *Config
	logFile    *os.File
	cache      *Cache
)

func Log(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

func main() {
	flag.StringVar(&configFile, "conf", "", "configuration file")
	flag.Parse()

	config = LoadConfig(configFile)
	log.Printf("Start listening: %s", config.Server.Listen)
	log.SetOutput(GetLogFile(config.Server.Log))

	cache = NewCache()

	http.HandleFunc("/", mainHandler)
	http.ListenAndServe(config.Server.Listen, Log(http.DefaultServeMux))
}

func mainHandler(w http.ResponseWriter, r *http.Request) {
	request_path := r.URL.Path[1:]

	re, err := regexp.Compile(config.Filter.Regex)
	if err != nil {
		log.Fatal(err)
	}

	matches := re.FindStringSubmatch(request_path)
	if len(matches) == 0 {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	var exerr *FilterError
	var request *Request

	request, exerr = NewRequest(matches)
	if exerr != nil {
		http.Error(w, exerr.Message, exerr.Code)
		return
	}

	request.Execute(w, r)
}

func GetLogFile(name string) *os.File {
	var file *os.File
	var err error

	if _, err := os.Stat(path.Dir(name)); err != nil {
		err := os.MkdirAll(path.Dir(name), 0755)
		if err != nil {
			log.Printf("can't create log path %v\n", path.Dir(name))
		}
	}

	file, err = os.OpenFile(name, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
	if err != nil {
		log.Printf("can't open logfile %v\n", name)
	}

	return file
}
