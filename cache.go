package main

import (
	"io/ioutil"
	"log"
	"os"
	"path"
)

type Cache struct{}

func (c *Cache) Has(filePath string) bool {
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		log.Printf("file not in cache %v", filePath)
		return false
	}

	return true
}

func (c *Cache) Get(filePath string) []byte {
	result, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(err)
	}

	return result
}

func (c *Cache) Add(filePath string, blob []byte) {
	var err error

	err = os.MkdirAll(path.Dir(filePath), 0755)
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile(filePath, blob, 0755)
	if err != nil {
		panic(err)
	}
	
	log.Printf("cached %v", filePath)
}

func NewCache() *Cache {
	cache := new(Cache)

	return cache
}
